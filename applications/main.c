/*
 * Copyright (c) 2006-2020, RT-Thread Development Team
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs:
 * Date           Author       Notes
 * 2020-09-02     RT-Thread    first version
 */

#include <rtthread.h>
#include <rtdevice.h>
#include "drv_common.h"
#include "app_oled.h"
#include "app_rtc.h"
#include "app_key.h"
#include "app_sensor.h"
#include "app_step.h"
#include "app_myiic.h"
#include "app_compass.h"

#define LED1_PIN GET_PIN(I, 8)

extern void wlan_autoconnect_init(void);
static int ThreadInit(void);

/*********************************定义LED任务的相关参数**************************************/
#define LED1_Priority       10
#define LED1_TimeSlices     5
static struct rt_thread     led1_thread;            // 线程控制块
ALIGN(RT_ALIGN_SIZE)
static rt_uint8_t           rt_led1_thread_stack[256];    //线程栈
static void Led1_ThreadEntry(void* parameter);     //线程LED1
/*********************************定义OLED任务的相关参数**************************************/
#define OLED_Priority       7
#define OLED_TimeSlices     5
static struct rt_thread     oled_thread;            // 线程控制块
ALIGN(RT_ALIGN_SIZE)    //栈对齐方式
static rt_uint8_t           rt_oled_thread_stack[2048];    //线程栈
/*********************************定义OLED任务的相关参数**************************************/
#define RTC_Priority        8
#define RTC_TimeSlices      5
static struct rt_thread     rtc_thread;            // 线程控制块
ALIGN(RT_ALIGN_SIZE)    //栈对齐方式
static rt_uint8_t           rt_rtc_thread_stack[512];    //线程栈
/*********************************定义KEY任务的相关参数**************************************/
#define KEY_Priority        5
#define KEY_TimeSlices      5
static struct rt_thread     key_thread;            // 线程控制块
ALIGN(RT_ALIGN_SIZE)    //栈对齐方式
static rt_uint8_t           rt_key_thread_stack[512];    //线程栈
/*********************************定义pedometer任务的相关参数**************************************/
#define Pedometer_Priority        4
#define Pedometer_TimeSlices      5
static struct rt_thread     pedometer_thread;            // 线程控制块
ALIGN(RT_ALIGN_SIZE)    //栈对齐方式
static rt_uint8_t           rt_pedometer_thread_stack[512];    //线程栈

int main(void)
{
    rt_uint32_t count = 1;

    rt_pin_mode(LED1_PIN, PIN_MODE_OUTPUT);
    IIC_Init();
    /* init Wi-Fi auto connect feature */
    wlan_autoconnect_init();
    /* enable auto reconnect on WLAN device */
    rt_wlan_config_autoreconnect(RT_TRUE);

    (void)ThreadInit();

    while(count++)
    {
        ;
    }
    return RT_EOK;
}

/*
 * Thread Initial
 * */
static int ThreadInit(void)
{
    rt_err_t tmp_result1 = 0;
    rt_err_t tmp_result2 = 0;
    rt_err_t tmp_result3 = 0;
    rt_err_t tmp_result4 = 0;
    rt_err_t tmp_result5 = 0;

    // 创建静态线程
    tmp_result1 = rt_thread_init(&led1_thread,                 //线程控制块
                               "led1_blink",                 //线程名字，在shell里面可以看到
                               Led1_ThreadEntry,            //线程入口函数
                               RT_NULL,                      //线程入口函数参数
                               &rt_led1_thread_stack[0],     //线程栈起始地址
                               sizeof(rt_led1_thread_stack), //线程栈大小
                               LED1_Priority,                //线程的优先级
                               LED1_TimeSlices);             //线程时间片
    // 创建静态线程
    tmp_result2 = rt_thread_init(&oled_thread,                //线程控制块
                               "oled_display",               //线程名字，在shell里面可以看到
                               OLED_ThreadEntry,             //线程入口函数
                               RT_NULL,                      //线程入口函数参数
                               &rt_oled_thread_stack[0],     //线程栈起始地址
                               sizeof(rt_oled_thread_stack), //线程栈大小
                               OLED_Priority,                //线程的优先级
                               OLED_TimeSlices);             //线程时间片

    // 创建静态线程
    tmp_result3 = rt_thread_init(&rtc_thread,                //线程控制块
                               "rtc_time",               //线程名字，在shell里面可以看到
                               RTC_ThreadEntry,             //线程入口函数
                               RT_NULL,                      //线程入口函数参数
                               &rt_rtc_thread_stack[0],     //线程栈起始地址
                               sizeof(rt_rtc_thread_stack), //线程栈大小
                               RTC_Priority,                //线程的优先级
                               RTC_TimeSlices);             //线程时间片

    // 创建静态线程
    tmp_result4 = rt_thread_init(&key_thread,                //线程控制块
                               "key_control",               //线程名字，在shell里面可以看到
                               KEY_ThreadEntry,             //线程入口函数
                               RT_NULL,                      //线程入口函数参数
                               &rt_key_thread_stack[0],     //线程栈起始地址
                               sizeof(rt_key_thread_stack), //线程栈大小
                               KEY_Priority,                //线程的优先级
                               KEY_TimeSlices);             //线程时间片

    // 创建静态线程
    tmp_result5 = rt_thread_init(&pedometer_thread,                //线程控制块
                               "pedometer_control",               //线程名字，在shell里面可以看到
                               Pedometer_ThreadEntry,             //线程入口函数
                               RT_NULL,                      //线程入口函数参数
                               &rt_pedometer_thread_stack[0],     //线程栈起始地址
                               sizeof(rt_pedometer_thread_stack), //线程栈大小
                               Pedometer_Priority,                //线程的优先级
                               Pedometer_TimeSlices);             //线程时间片



    if(RT_EOK == tmp_result1)
    {
        rt_thread_startup(&led1_thread);             //启动线程led0_thread，开启调度
    }
    else
    {
        return -RT_ENOMEM;
    }

    if(RT_EOK == tmp_result2)
    {
        rt_thread_startup(&oled_thread);             //启动线程led0_thread，开启调度
    }
    else
    {
        return -RT_ENOMEM;
    }

    if(RT_EOK == tmp_result3)
    {
        rt_thread_startup(&rtc_thread);             //启动线程led0_thread，开启调度
    }
    else
    {
        return -RT_ENOMEM;
    }

    if(RT_EOK == tmp_result4)
    {
        rt_thread_startup(&key_thread);             //启动线程led0_thread，开启调度
    }
    else
    {
        return -RT_ENOMEM;
    }

    if(RT_EOK == tmp_result5)
    {
        rt_thread_startup(&pedometer_thread);             //启动线程led0_thread，开启调度
    }
    else
    {
        return -RT_ENOMEM;
    }

    return RT_EOK;
}

/*
 * LED 0 thread
 */
static void Led1_ThreadEntry(void* parameter)
{
//    uint64_t tmp_led1_counter = 0u;

    rt_pin_mode(LED1_PIN, PIN_MODE_OUTPUT);    //初始化LED1引脚

    while (1)
    {
        rt_pin_write(LED1_PIN, rt_pin_read(LED1_PIN)^1);
        rt_thread_mdelay(500);
//        rt_kprintf("thread led1 count: %d\r\n",  tmp_led1_counter ++); /*打印线程计数值输出 */
//        rt_kprintf("LED1_thread 线程被挂起!\r\n");
//        rt_thread_suspend(&led1_thread);    //挂起线程
//        rt_schedule();                      //线程调度
//        rt_thread_resume(&led0_thread);     //恢复线程1
//        rt_kprintf("LED_thread 线程被解挂!\r\n");
    }
}



#include "stm32h7xx.h"
static int vtor_config(void)
{
    /* Vector Table Relocation in Internal QSPI_FLASH */
    SCB->VTOR = QSPI_BASE;
    return 0;
}
INIT_BOARD_EXPORT(vtor_config);


